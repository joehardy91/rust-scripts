mod oshelp;
extern crate parser;
extern crate time;
//extern crate regex;
#[allow(unconventional_style)]

use std::env;
use std::path::Path;
use std::fs;
//use std::io::BufReader;
//use std::str;
use parser::mjrparser;
use parser::dl4parser;
use parser::pdatastruct;
//use regex::Regex;

fn determine_graph_path(masterdirpath:String,fpath:&str)->String{
	//returns spath and creates folder if necessary
	let mut finbuf=vec![];
	let mut lastcomp = vec![];
	let mut mdpcomp = Path::new(&masterdirpath).components();
	let mut fpathcomp = Path::new(fpath).components();
	for comp in fpathcomp{
		let u = mdpcomp.next();
		if u.is_some(){
			if comp.as_os_str()==u.unwrap().as_os_str(){
				println!("{:?}",comp.as_os_str());
				finbuf.push(Path::new(comp.as_os_str()));
			} else {
				println!("{:?}",comp.as_os_str());
				lastcomp.push(Path::new(comp.as_os_str()));
				break;
			} 
		} else {
				println!("{:?}",comp.as_os_str());
				lastcomp.push(Path::new(comp.as_os_str()));
				break;
		}
	}
	
	let parentpath_buf = Path::new("");
	
	for st in finbuf{
		parentpath_buf.join(Path::new(st));
	}
	println!("{:?}",parentpath_buf);
	//let mut parentpath = Path::new(parentpath_buf.as_os_str());
	if lastcomp.len()>0{
		parentpath_buf.join(Path::new(lastcomp[0]));
	}
	let mut parentpath = Path::new(parentpath_buf.as_os_str());
	parentpath.to_str().unwrap().to_string()
}

fn main() {
	let time1 = time::now_utc();
    let args: Vec<_> = env::args().collect();
	let res = match args.len()>1{
			true => &args[1],
			false => panic!("No file path entered!"),
	};
	
	
	
	let masterdirpath = Path::new(res);
	let mastergraphpath_buf = masterdirpath.join(Path::new("graphData"));
	let mastergraphpath = mastergraphpath_buf.as_path();
	
	// if mastergraphpath.is_dir(){
		// fs::remove_dir_all(mastergraphpath).expect("Could not remove graph directory");
	// } else {
		// fs::create_dir(mastergraphpath).expect("Could not create graph directory");
	// }
	
	//println!("{:?}",graphpath);
	let mut filevec: Vec<String> = Vec::new();
	oshelp::os::visit_dirs(&masterdirpath,&mut filevec).expect("Oh no!");
	let mut total_data:Vec<pdatastruct::parserdata>=vec![];
	for file in &filevec{
		let fpath=Path::new(file);
		let gpath = determine_graph_path(masterdirpath.as_os_str().to_str().unwrap().to_string(),&file);
		println!("{:?},{:?}",gpath,fpath);
		if fpath.extension().unwrap().to_str().unwrap().to_lowercase().as_str()=="mjr"{
			let f = fs::File::open(fpath).expect("oh no!");
			let (data,graphData) = mjrparser::parse(f,fpath.to_str().unwrap());
			total_data.push(data);
		} else if fpath.extension().unwrap().to_str().unwrap().to_lowercase().as_str()=="dl4"{
			let f = fs::File::open(fpath).expect("oh no!");
			let (data,graphData) = dl4parser::parse(f,fpath.to_str().unwrap());
			total_data.push(data);
		}
		
	}
	let running_ms = (time::now_utc()-time1).num_milliseconds();
	println!("{}.{:03} seconds;{} ms",running_ms/1000,running_ms%1000,running_ms);
}
