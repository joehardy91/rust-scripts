//Rust rebuild of the datalogger parser
//os module
//a function to collect child entities of parent os entity(file,folders)



pub mod os{
	use std::io;
	use std::fs::{self};
	use std::path::Path;
	// one possible implementation of walking a directory only visiting files
	pub fn visit_dirs(dir: &Path,mut filevec:&mut Vec<String>) -> io::Result<()> {
		if try!(fs::metadata(dir)).is_dir() {
			for entry in try!(fs::read_dir(dir)) {
				let entry = try!(entry);
				if try!(fs::metadata(entry.path())).is_dir() {
					try!(visit_dirs(&entry.path(),filevec));
				} else{
					let mres = match entry.path().extension(){
						Some(_)=>true,
						None =>false,
					};
					if mres{
						match entry.path().extension().unwrap().to_str().unwrap().to_lowercase().as_str(){
							"mjr"|"dl4"|"dl5"=>filevec.push(entry.path().to_str().unwrap().to_string()),
							_=>{},
						}
					}
					
				}
			}
		}
		Ok(())
		
	}
	// pub fn delete_dir_and_contents(dir: &Path)->io::Result<()>{
		// try!(fs::remove_dir_all(dir));
		// Ok(())
	// }
	// pub fn print_dir(x:&DirEntry){
		// println!("{:?}",x.path());    
    // }
	// fn push_path_to_vec(p:&DirEntry,mut v:Vec<String>){
		// v.push(p.path().to_str().unwrap().to_string());
	// }

}
	

//fn main() {
    //println!("Hello, world!");
    // let spath = Path::new("Z:\\017 Salt Lake City\\NEW REPORTS\\04.08.2016");
    //println!("{:?}",&spath.as_os_str());
	// let mut filevec: Vec<String> = Vec::new();
	// let fv = os::visit_dirs(&spath,&mut filevec).expect("Oh no!");
	// for item in &filevec{
		// println!("{:?}",item);
	//}
	// match r{
		// Ok(fv)=>{},
		// Err(_)=>println!("There was an error in collecting files."),
	// }
	// r.
	
	
//}
