fn print_number(x:i32){
	println!("the variable is {}", x);
}

fn print_sum(x:i32,y:i32){
	println!("sum is {}", x+y );
}

fn add_one(x:i32)->i32{
	x+1
}

fn main() {

    // let mut x:i32 = 5;
    // print_number(x);
    // x=add_one(x);
    // print_sum(x,x);
    // //let z: String = diverges();

    // let f: fn(i32) -> i32 = add_one;

    // //let f = add_one;
    // let six = f(32);
    // print_number(six);

    //BOOLEANS
    let x = true;
    let y: bool = false;

    //CHARACTERS(unicode scalar value of 4 bytes)
    let x = 'x';
    let two_hearts = '💕';

    //NUMBERS
    let x = 42; //x has type i32
    let y: f64 = 1.0; //y has type f64
    let z = 1;
    //u stores only positive values(0,(size)^2-1), i stores neg and positive values from (-2*size,2*size-1)
    //isize and usize are flexible to the specifications of the underlying machine

    //Arrays(fixed size list of elements of the same type). Arrays have type [Type; Num_Elements]
    let a = [1,2,3]; //a: [i32;3]
    let mut m = [1,2,3]; //m: [i32;3]

    //can also initialize in this way:
    let a2 = [0;20]; //a2: [i32;20] 20 elements of value 0
    println!("A2 has length {}. The {}th element of A2 is {}.",a2.len(),z,a2[z]);

    let names = ["Graydon","Brian","Niko"];//names: [&str;3]

    println!("The second name is: {}", names[1]);

    //slicing
    let ab = [0,1,2,3,4];
    let complete = &ab[..];
    let middle = &ab[1..4]; //type &[isize]

    //tuples(ordered list of fixed size)
    let x = (1,"hello");
    let x: (i32,&str) = (1,"hello");

    let mut x1=(1,2);
    let y1 = (2,3);

    x1=y1;

    let (x2,y2,z2) = (1,2,3);

    print_number(z2);

    (0,); //single element tuple
    (0); //zero in parentheses

    //tuple indexing
    let tuple = (1,2,3);

    let x = tuple.0;
    let y = tuple.1;
    let z = tuple.2;

    print_number(y);

    //functions
    fn foo(x:i32) -> i32{x}

    let x:fn(i32)->i32 = foo;
    let x = 5;
    //special if structures
    let mut y = if x == 5 {10} else {15}; //y: i32
    let mut done = false;
    while !done{
    	y+=y-3;

    	println!("{}", y);

    	if y%5==0{
    		done=true;
    	}
    }
    print_number(y);


for z_i in 0..10 {
	println!("{}", z_i); //z_i:i32
	}
for (i,j) in (5..10).enumerate(){
	println!("i = {} and j = {}", i, j);
	}
let lines=vec![1,2,3].into_iter();
for (linenumber, line) in lines.enumerate(){
	println!("{}:{}", linenumber,line);
}

}

fn diverges() -> ! {
	panic!("This function never returns!");
}
