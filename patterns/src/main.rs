

fn main() {
    let x = 1;
    let c = 'c';
    match c{
    	//x=>println!("x: {} c: {}",x,c),
    	//y=>println!("y: {} c: {}",y,c),
    	_=>println!("2"),
    }
    println!("x: {:?}", x);

    match x{
    	1|2 => println!("one or two"),
    	3 => println!("three"),
    	_=>println!("anything"),
    }
    //we can match only some of the values
    let origin = Point {x:4,y:0};

    match origin{
    	Point{x:3,..}=>println!("x is {:?}", origin.x),
    	Point{..}=>println!("y is {}",origin.y),
    }
    match origin{
    	Point{x,..}=>println!("x is {}",x ),
    }

    //result checking
    // match sv{
    // 	Ok(value)=>println!("got a value"),
    // 	Err(err)=>println!("{:?}",err ),
    // }

    match x{
    	1 ... 5 => println!("one through five"),
    	_=>println!("anything"),
    }

    let example_char = '💅';
    match example_char{
    	'a' ... 'j' => println!("early letter"),
    	'k' ... 'z' => println!("late letter"),
    	_=>println!("something else"),
    }

    match x {
    	e @ 1 ... 5 => println!("got a range element {:?}",e ),
    	_=>println!("anything"),
    }

    //complicated match of part of a data structure
    //#[derive(Debug)]
    struct Person{
    	name: Option<String>,
    }

    let name = "Steve".to_string();
    let x2: Option<Person> = Some(Person{name:Some(name)});
    match x2 {
    	Some(Person{name:ref a @ Some(_), .. }) => println!("{:?}", a),
    	_=>{}
    }

    //guards (match guards)
    enum OptionalInt{
    	Value(i32),
    	Missing,
    }

    let z = OptionalInt::Value(5);

    match z{
    	OptionalInt::Value(i) if i > 5 =>println!("Got an int bigger than five!"),
    	OptionalInt::Value(..) => println!("Got an int!"),
    	OptionalInt::Missing => println!("No such luck."),
    }

    let z2 = 4;
    let z3 = false;
    match z2{
    	4|5 if z3 => println!("yes"),
    	_ => println!("no"),
    }
}

struct Point {
	x: i32,
	y: i32,
}