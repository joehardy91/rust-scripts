// use std::sync::Arc;
// //immutabililty implies 'exterior mutability'(aka gives &T), not that something can't be changed
// fn main() {
//     let x = Arc::new(5);
//     let y = x.clone();
// }

//std::cell has interior mutability(aka gives &mut T)

use std::cell::RefCell;
let x = RefCell::new(42);
let y = x.borrow_mut();


//structs cannot mix mutable and immutable fields; their immutability is assigned at binding
struct Point{
	x:i32,
	y:i32,
}

let mut a = Point{x:5,y:6}
let b = Point{x:2,y:3}

a.x=10; //fine

b.x=10; //error

//by using Cell<T>, you can emulate field-level mutability

use std::cell::Cell;
struct Point{
	x:i32,
	y:Cell<i32>,
}

let point = Point {X:5,y:Cell::new(6)};

point.y.set(7);

println!("y: {:?}", point.y);