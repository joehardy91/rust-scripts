struct Foo<'a>{
	x: &'a i32,
}

impl<'a> Foo<'a>{
	fn x(&self) -> &'a i32 {self.x}
}

fn x_or_y<'a,'b>(x: &'a str, y:&'b str) -> &'a str {x}

fn main() {
	let y = 5; //same as let _y=5; let y = &_y;
	let f = Foo {x: &y };

	println!("x is {},{}", f.x(),x_or_y(&"ok",&"yes"));
}
