use std::net::TcpStream;


fn main() {
	//str strings are called string slices. they cannot be mutated. they are a sequence of utf-8 bytes
    let greeting = "Hello there."; //greeting: &'static str

    let s="foo\
    bar";

    assert_eq!("foobar",s );

    //a String is a heap-allocated string. the string is growable, and is also guaranteed to be UTF-8.
    //Strings are commonly created by converting from a string slice using the to_string method

    let mut s1 = "Hello".to_string(); //mut s:String

    println!("{:?}", s1);

    s1.push_str(", world.");
    println!("{}", s1);

    //Strings will coerce into &str with an &
	takes_slice(&s1);

	//Strings must be explicitly converted using &*

	TcpStream::connect("192.168.0.1:3000"); //&str parameter

	let addr_string = "192.168.0.1:3000".to_string();
	TcpStream::connect(&*addr_string); //convert addr_string to &str

	//strings do not support indexing

	let hachiko = "忠犬ハチ公";

	for b in hachiko.as_bytes() {
	    print!("{}, ", b);
	}

	println!("");

	for c in hachiko.chars() {
	    print!("{}, ", c);
	}

	println!("");

	let dog = hachiko.chars().nth(1);
	let dog1 = "hachiko";
	let hachi = &dog1[0..2];

	let hello_world = "Hello ".to_string() + "world!";
	let hello_world2 = "Hello ".to_string() + &"world!".to_string();
}
fn takes_slice(slice:&str){
	println!("Got: {}", slice);
}