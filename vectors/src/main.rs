fn main() {
    let v = vec![1,2,3,4,5];//v:Vec<i32>
	let v1 = vec![0;10]; //ten zeroes
	{
		let k = &v;
		for (i,j) in k.into_iter().enumerate() {
				println!("Element {} of v is {:?}", i, j);
		}
	}
	//indexing must be done with usize type

	let i: usize = 3;
	match v.get(i){
		Some(x) => println!("Item {} is {}", i+1,x ),
		None => println!("Sorry, this vector is too short."),
	}
	//let i_NO: i32 = 0;
	for element in &v{
		println!("A reference to {}", element);
	}
	//v[i_a];
	//NO v[i_NO]
}
