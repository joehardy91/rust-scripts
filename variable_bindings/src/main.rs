fn main() {
    // let x = 5; //x: i32
    // let mut y = 10;
    // y=x+y;
    // println!("{}",y );
    // let x: i32 = 17;
    // {
    // 	let y: i32 = 3;
    // 	println!("The value of x is {} and the value of y is {}", x, y );
    // }
    // //println!("The value of x is {} and value of y is {}", x, y);

    // let x: i32 = 8;
    // {
    // 	println!("{}", x);

    // 	let x = 12;

    // 	println!("{}", x);
    // }
    // println!("{}", x);
    // let x = 42;
    // println!("{}", x);

    let mut x: i32 = 1;
    x = 7;
    let x = x; //x is now immutable and bound to 7

    let y = 4;
    let y = "I can also be bound to text."; //y is now a different type
}
