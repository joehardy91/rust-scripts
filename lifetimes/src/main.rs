
// Here are the three rules:

// Each elided lifetime in a function’s arguments becomes a distinct lifetime parameter.

// If there is exactly one input lifetime, elided or not, that lifetime is assigned to all elided lifetimes in the return values of that function.

// If there are multiple input lifetimes, but one of them is &self or &mut self, the lifetime of self is assigned to all elided output lifetimes.

// Otherwise, it is an error to elide an output lifetime.
fn main() {
    //the lifetime of a variable is defined by its scope implicitly, or can be explicitly defined as below
    //implicit
    fn foo(x: &i32)->i32{*x}
    //explicit
    fn bar<'a>(x:&'a i32)->i32{*x}

    //two reference parameter
    fn baz<'a, 'b>(x: &'a i32,y:&'b i32)->i32{*x+y}

    let y = &5;
    //let z :'b i32 = 2;
    let f = Foo {x:y};
    let z:& i32 = &2;
    println!("{}", f.x());
    println!("{}", foo(y));
    println!("{}", bar(y));
    println!("Baz is {}", baz(z,y));


}
//structs with references need explicit lifetimes(you do not need to have references necessarily)
    struct Foo<'a>{
    	x:&'a i32,
    }
//impl implemenets methods on structures
impl<'a> Foo<'a>{
	fn x(&self)->&'a i32{self.x}
}
//the rule is to try to use lifetimes when using references