
use parserhelp;
use pdatastruct::parserdata;
use std::fs;
use parserhelp::general_byte_reading as gbr;
use parserhelp::mjr_byte_reading as mbr;
use std::io::BufReader;

use regex::Regex;

fn formatPipeSize(bv:&str,bt:u16,ev:&str,et:u16)->String{
	let beginning = match bt{
		1 => format!("{}\" DIPS ",bv),
		2 => format!("{}\" O.D. ",bv),
		3 => format!("{}mm O.D. ",bv),
		4 => format!("{}mm JIS ",bv),
		_ => format!("{}\" IPS ",bv),
	};

	let ending = match et{
		1=>format!("{}\" WT",ev),
		2=>format!("{}mm WT",ev),
		_ => format!("DR {}",ev),
	};
	
	
	beginning.to_string()+&ending
}

pub fn parse(f:fs::File,fpath:&str)->(parserdata,Vec<Vec<u16>>){
	let mut pdata = parserdata{..Default::default()};
	let mut reader = BufReader::new(f);
	pdata.FileVersion = Some(gbr::cleanutf8(gbr::readString(&mut reader,10)));
	let year = gbr::readShort(&mut reader);
	let month = gbr::readShort(&mut reader);
	let day = gbr::readShort(&mut reader);
	let hour = gbr::readShort(&mut reader);
	let minute = gbr::readShort(&mut reader);
	let second = gbr::readShort(&mut reader);
	
	pdata.DateTime = Some(format!("{:04}/{:02}/{:02} {:02}:{:02}:{:02}",year,month,day,hour,minute,second));
	
	pdata.MachID = Some(mbr::readMJRString(&mut reader));
	
	{
	let mut junk = gbr::readShort(&mut reader);
	}
	
	pdata.MachModel = Some(mbr::readMJRString(&mut reader));
	
	pdata.PistonArea = Some(mbr::readMJRString(&mut reader));
	
	{
	let mut junk = gbr::readShort(&mut reader);
	}
	
	pdata.Operator = Some(mbr::readMJRString(&mut reader));
	
	pdata.Job_No = Some(mbr::readMJRString(&mut reader));
	
	pdata.Joint_No = Some(format!("{}",gbr::readShort(&mut reader)));
	
	{
	let mut junk = gbr::readShort(&mut reader);
	}
	
	pdata.PipeMaterial = Some(mbr::readMJRString(&mut reader));
	
	let sidewinderregex = Regex::new(r"(Sidewinder Mech.)").unwrap();
	let swregex = Regex::new(r". SW").unwrap();
	
	let machModelT = &pdata.MachModel.clone().unwrap();
	//println!("{:?}", sidewinderregex.is_match(machModelT) || swregex.is_match(machModelT));
	if sidewinderregex.is_match(machModelT) || swregex.is_match(machModelT){
		pdata.FittingType = Some(mbr::readMJRString(&mut reader));
		{
		let mut junk = gbr::readShort(&mut reader);
		}
		pdata.PipeSize = Some(mbr::readMJRString(&mut reader));
		{
		let mut junk = gbr::readShort(&mut reader);
		}	
	} else {
	
		let pipeSizeBeginValue = mbr::readMJRString(&mut reader);
		let pipeSizeBeginType = gbr::readShort(&mut reader);
		let pipeSizeEndValue = mbr::readMJRString(&mut reader);
		let pipeSizeEndType = gbr::readShort(&mut reader);
		
		pdata.PipeSize=Some(formatPipeSize(&pipeSizeBeginValue,pipeSizeBeginType,&pipeSizeEndValue,pipeSizeEndType));
	}
	pdata.BeadIFP = Some(mbr::readMJRString(&mut reader));
	pdata.SoakIFP = Some(mbr::readMJRString(&mut reader));
	pdata.FuseIFP = Some(mbr::readMJRString(&mut reader));
	pdata.CoolIFP = Some(mbr::readMJRString(&mut reader));

	{
		let mut junk = gbr::readShort(&mut reader);
	}

	pdata.DragPSI = Some(mbr::readMJRString(&mut reader));
	pdata.BeadPSI = Some(mbr::readMJRString(&mut reader));
	pdata.SoakPSI = Some(mbr::readMJRString(&mut reader));
	pdata.FusePSI = Some(mbr::readMJRString(&mut reader));
	pdata.CoolPSI = Some(mbr::readMJRString(&mut reader));

	{
		let mut junk = gbr::readShort(&mut reader);
	}
	
	pdata.External_Probe = Some(mbr::readMJRString(&mut reader));;
	pdata.Datalogger_Probe = Some(mbr::readMJRString(&mut reader));;
	
	{
		for x in 0..4{
		let mut junk = gbr::readShort(&mut reader);
		}
	}
	
	pdata.Notes = Some(mbr::readMJRString(&mut reader));
	if pdata.Notes==Some("".to_string()){
		let mut junk = gbr::readShort(&mut reader);
	}

	
	{
		let mut junk = gbr::readShort(&mut reader);
	}

	let loggerInfo = mbr::readMJRString(&mut reader);


	
	pdata.Datalogger_No = Some(loggerInfo[11..15].to_string());

	pdata.Software = Some(format!("v.{}.{}.{}",&loggerInfo[15..16],&loggerInfo[16..17],&loggerInfo[17..18]));

	pdata.Calibration_Date=Some(format!("{}/{}/{}",&loggerInfo[18..22],&loggerInfo[22..24],&loggerInfo[24..26]));

	{
		let mut junk = mbr::readMJRString(&mut reader);
	}

	let graphHeader = gbr::cleanutf8(gbr::readString(&mut reader,20));

	pdata.MaxGraphPSI = Some(graphHeader[2..6].trim().to_string());
	pdata.MaxGraphTime = Some(graphHeader[8..13].trim().to_string());
	pdata.GraphNumPoints = Some(graphHeader[15..20].trim().to_string());

	{
		let mut junk = gbr::readString(&mut reader,1);
	}

	let ngp = pdata.GraphNumPoints.clone().unwrap().parse().expect("No!!!");
	let mut graphData = Vec::new();
	for k in 0..ngp{
		let t = gbr::readShort(&mut reader);
		let p = gbr::readShort(&mut reader);
		graphData.push(vec![t,p]);
	}
	pdata.Path_to_data_file=Some(fpath.to_string());

	//println!("{:?}",pdata);
	(pdata,graphData)
}