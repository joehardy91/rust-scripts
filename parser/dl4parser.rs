
use parserhelp;
use pdatastruct::dl4graphdataline;
use pdatastruct::parserdata;
use std::fs;
use parserhelp::general_byte_reading as gbr;
use parserhelp::dl4_byte_reading as dl4;
use std::io::BufReader;

use regex::Regex;

pub fn parse(f:fs::File,fpath:&str)->(parserdata,Vec<Vec<u32>>){
	let mut pdata = parserdata{..Default::default()};
	let mut reader = BufReader::new(f);
	
	pdata.FileVersion = Some(dl4::readDL4String(&mut reader));
	let (s1,s2,copyright) = (Some(dl4::readBitFlipString(&mut reader)),Some(dl4::readBitFlipString(&mut reader)),Some(dl4::readBitFlipString(&mut reader)));
	
	pdata.DateTime = Some(dl4::readBitFlipString(&mut reader));
	pdata.Operator = Some(dl4::readBitFlipString(&mut reader));
	pdata.Job_No = Some(dl4::readBitFlipString(&mut reader));
	
	pdata.Joint_No=Some(format!("{}",gbr::readInt(&mut reader)));
	
	pdata.MachID = Some(dl4::readBitFlipString(&mut reader));
	pdata.MachModel = Some(dl4::readBitFlipString(&mut reader));
	pdata.PistonArea = Some(dl4::readBitFlipString(&mut reader));
	
	let Ftype = dl4::readBitFlipString(&mut reader);
	
	let ftypebreakdown:Vec<&str> = Ftype.split('\r').collect();
	
	pdata.FusionType=Some(ftypebreakdown[0].trim().to_string());
	
	if ftypebreakdown.len()>1{
		pdata.FusionStandard=Some(ftypebreakdown[1].trim().to_string());
	}
	
	let PInfo = dl4::readBitFlipString(&mut reader);
	
	let pinfobreakdown:Vec<&str>=PInfo.split('\r').collect();
	
	pdata.PipeMaterial = Some(pinfobreakdown[0].trim().to_string());

	if pinfobreakdown.len()>1{
		pdata.PipeSize=Some(pinfobreakdown[1].trim().to_string());
	}
	if pinfobreakdown.len()>2{
		pdata.PipeAngle=Some(pinfobreakdown[2].trim().to_string());
	}
	
	{
	let mut junk = gbr::readShort(&mut reader);
	}
	
	{
	let mut pressureUnit = dl4::readBitFlipString(&mut reader);
	}
	
	pdata.BeadIFP = Some(dl4::readBitFlipString(&mut reader));
	pdata.SoakIFP = Some(dl4::readBitFlipString(&mut reader));
	pdata.FuseIFP = Some(dl4::readBitFlipString(&mut reader));
	pdata.CoolIFP = Some(dl4::readBitFlipString(&mut reader));
	pdata.BeadPSI = Some(dl4::readBitFlipString(&mut reader));
	pdata.SoakPSI = Some(dl4::readBitFlipString(&mut reader));
	pdata.FusePSI = Some(dl4::readBitFlipString(&mut reader));
	pdata.CoolPSI = Some(dl4::readBitFlipString(&mut reader));
	
	pdata.Heater = Some(dl4::readBitFlipString(&mut reader));
	pdata.MinBead = Some(dl4::readBitFlipString(&mut reader));
	let SoakTime = Some(dl4::readBitFlipString(&mut reader));
	pdata.DwellTime = Some(dl4::readBitFlipString(&mut reader));
	let BeadTime = Some(dl4::readBitFlipString(&mut reader));
	let FuseCoolTime = Some(dl4::readBitFlipString(&mut reader));
	
	pdata.DragPSI = Some(dl4::readBitFlipString(&mut reader));
	pdata.MaxGraphPSI = Some(format!("{}",gbr::readShort(&mut reader)));
	let ngp = gbr::readInt(&mut reader);
	pdata.GraphNumPoints = Some(format!("{}",ngp));
	let mut graphData = Vec::new();
	
	for k in 0..ngp{
		let t = gbr::readInt(&mut reader);
		let mut p = gbr::readShort(&mut reader) as u32;
		if p>32768{
			let j = 65536-p;
			p=j;
		}
		graphData.push(vec![t,p]);
	}
	
	pdata.Notes = Some(dl4::readBitFlipString(&mut reader));
	let startTime = Some(dl4::readBitFlipString(&mut reader));
	pdata.Computer = Some(dl4::readBitFlipString(&mut reader));
	
	let dnuU = dl4::readBitFlipString(&mut reader);
	let dnu:Vec<&str> = dnuU.split("-").collect();
	if dnu.len()>1{
		pdata.Datalogger_No = Some(dnu[1].to_string());
	} else {
		pdata.Datalogger_No = Some(dnu[0].to_string());
	}
	
	
	pdata.Calibration_Date = Some(dl4::readBitFlipString(&mut reader));
	pdata.Firmware = Some(dl4::readBitFlipString(&mut reader));
	
	{
		let junk = gbr::readInt(&mut reader);
	}
	
	pdata.External_Probe = Some(dl4::readBitFlipString(&mut reader));
	pdata.Path_to_data_file=Some(fpath.to_string());
	
	
	//println!("{:?}",pdata);
	
	(pdata,graphData)
	
	
}