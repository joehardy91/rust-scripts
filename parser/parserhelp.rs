

pub mod general_byte_reading{

		use std::io;
		use std::io::prelude::*;
		use std::str;
		use byteorder::{ByteOrder,LittleEndian,ReadBytesExt,BigEndian};
		
		pub fn cleanutf16(k:io::Result<Vec<u16>>)->String{
		
			let string = match k{
				Ok(x)=>x,
				Err(e)=>panic!("String read failed."),
			};
			
			let s = match String::from_utf16(string.as_slice()){
				Ok(v)=>v,
				Err(e)=>panic!("Invalid UTF-16 sequence:{}",e),
			};
			
			s
		}

		
		pub fn cleanutf8(k:io::Result<Vec<u8>>)->String{
		
			let string = match k{
				Ok(x)=>x,
				Err(e)=>panic!("String read failed."),
			};
			
			//let mut bufrdr = io::Cursor::new(string);
			
			//let string2 = bufrdr.read_u16::<LittleEndian>();
			
			let s = match String::from_utf8(string){
				Ok(v)=>v,
				Err(e)=>panic!("Invalid UTF-8 sequence:{}",e),
			};
			
			s
		}
		
		pub fn readString<R>(reader:R,num_elements: u64)->io::Result<Vec<u8>>
			where R: Read,
			{
			
			let mut buf = vec![];
			let mut chunk = reader.take(num_elements);
			let status = chunk.read_to_end(&mut buf);
			match status{
				Ok(n)=>assert_eq!(num_elements as usize,n),
				_ => panic!("Not enough bytes read!"),
				//later on install an error concatenation function.
			}
			
			

			// for c in buf{
				// c.to_le();
				
			// }
			Ok(buf)
		}
		
		pub fn readRawString<R>(mut reader: R,num_elements:u16)->io::Result<Vec<u16>> where R:Read,{
			let mut buf: Vec<u16> = Vec::new();

			for c in 0..num_elements{
				let k = reader.read_u16::<BigEndian>().unwrap();
				buf.push(k);
			}

			Ok(buf)
		}
		
		pub fn readFlippedString<R>(mut reader: R,num_elements:u32)->io::Result<Vec<u16>> where R:Read,{
			let mut buf: Vec<u16> = Vec::new();

			for c in 0..num_elements{
				let k = reader.read_u16::<LittleEndian>().unwrap();
				buf.push(!k);
			}

			Ok(buf)
		}
		
		pub fn readNextString<R>(mut reader:R,num_elements:u32)->io::Result<Vec<u16>> where R:Read,{
			let mut buf: Vec<u16> = Vec::new();
			for c in 0..num_elements{
				let k = reader.read_u16::<LittleEndian>().unwrap();
				buf.push(k);
			}

			Ok(buf)
		}
		
		pub fn readShort<R>(reader:R)->u16 where R:Read,{
		
			let mut buf = vec![];
			let mut chunk = reader.take(2);
			let status = chunk.read_to_end(&mut buf);
			match status{
				Ok(n)=>assert_eq!(2 as usize,n),
				_ => panic!("Not enough bytes read!"),
			}
			let y = LittleEndian::read_u16(&mut buf);
			y
		}
		
		pub fn readInt<R>(reader:R)->u32 where R:Read,{
			let mut buf=vec![];
			let mut chunk = reader.take(4);
			let status = chunk.read_to_end(&mut buf);
			match status{
				Ok(n)=>assert_eq!(4 as usize,n),
				_ => panic!("Not enough bytes read!"),
			}
			let Int = LittleEndian::read_u32(&mut buf);
			Int
		}
	}

	pub mod mjr_byte_reading{

		use std::io;
		use std::io::prelude::*;
		use std::str;
		use byteorder::{ByteOrder,LittleEndian,ReadBytesExt,BigEndian};
		use parserhelp::general_byte_reading as gbr;
		
		pub fn readMJRString<R>(mut reader:R)->String where R:Read,{
			let length = gbr::readShort(&mut reader);
			let string = gbr::cleanutf16(gbr::readRawString(reader, length));
			
			string
		}
		
	}

	pub mod dl4_byte_reading{
		use std::io;
		use std::io::prelude::*;
		use std::str;
		use byteorder::{ByteOrder,LittleEndian,ReadBytesExt,BigEndian};
		use parserhelp::general_byte_reading as gbr;
		
		pub fn readBitFlipString<R>(mut reader:R)->String where R:Read,{
			let length = gbr::readInt(&mut reader);
			let string = gbr::cleanutf16(gbr::readFlippedString(reader,length));
			
			string
		}
		
		pub fn readDL4String<R>(mut reader: R)->String where R:Read,{
			let length = gbr::readInt(&mut reader);
			let string = gbr::cleanutf16(gbr::readNextString(reader,length));
			
			string

		}
	}
