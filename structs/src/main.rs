//structs are ways of creating more complex data types

//for example, calculations involving coordinates in 2D space:

struct Point{
	x:i32,
	y:i32,	
}

impl Point{
	fn display(&self){
		println!("The point is at ({}, {})", self.x, self.y);
	}
	fn get_slope(&self,pt2:&Point)->f64{
		let (pt2y,pt2x,selfy,selfx) = (pt2.y as f64,pt2.x as f64,self.y as f64,self.x as f64);
		(pt2y-selfy)/(pt2x-selfx)
	}
	fn display_slope(&self,pt2:&Point){
		let slope = self.get_slope(pt2);
		println!("The value of the first point is ({},{}).
The value of the second point is ({},{}).
The slope between these two points is {}.",
			self.x,self.y,pt2.x,pt2.y,slope);
	}
}

//you can have a struct with a &mut pointer, which allows limited mutation
struct PointRef<'a>{
	x: &'a mut i32,
	y: &'a mut i32,
}

struct Point3D{
	x: i32,
	y: i32,
	z: i32,
}

//tuple structs are hybrids between those two data types. these structs have a name, but their fields don't
struct Color(i32,i32,i32);
struct tPoint(i32,i32,i32);

//tuple structs are most useful in the case they only have one element:
struct Inches(i32);

//structs can have no members at all. these are call unit-like structs
struct Electron;

//enums are sets of actions that isolate namespaces and enforce type safety. they are accessed via enum::variant
// let m = Message::Write("Hello,world",to_string())

// //this is equal to 
// fn foo(x:String)->Message{
// 	Message.Write(x)
// }

// let x = foo("Hello, world",to_string());

//this is useful in closures. you can pass functions as arguments to other functions. for example
//with iterators, we can do this(to convert a vector of Strings into a vector of Message::Writes):

// let v = vec!["Hello".to_string(),"World".to_string()];

// let v1: Vec<Message> = v.into_iter().map(Message::Write).collect();


//structs are camel cased with no underscores
fn main() {
    let mut origin = Point{x:0,y:0};

    origin.x = 5;

    //origin.display();

    let pt2 = Point{x:3,y:3};

    //pt2.display();

    //let e:f64 = origin.get_slope(&pt2);

    origin.display_slope(&pt2);
    {
    	let r = PointRef{x:&mut origin.x, y:&mut origin.y};

    	*r.x = 5;
    	*r.y = 6;
    }

    assert_eq!(5, origin.x );
    assert_eq!(6, origin.y );
    
    let mut point = Point3D{x:0,y:0,z:0};
    let mut o3D = Point3D{x:0,y:0,z:0};
    //you can update structs by using .. syntax to use every non-specified value
    point = Point3D{y:1,..point};
    //there is no need to use the same structure to use unspecified values
    let point=Point3D{z:1,x:2,..o3D};

    let black = Color(0,0,0);
    let origin2 = tPoint(0,0,0);

    println!("The red component of the color is equal to {:?}.", black.0);

    let length = Inches(10);
    let Inches(integer_length) = length;
    println!("length is {:?} inches", integer_length);

    let x = Electron;
}
