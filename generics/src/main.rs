//generics are called parametric polymorphisms in type theory,
//which means they are types of function that have multiple forms

//this is a generic type in the standard library:
// enum Option<T> {
//     Some(T),
//     None,
// }

//so is this
// enum Result<T, E> {
//     Ok(T),
//     Err(E),
// }
fn main() {
	let x: Option<i32> = Some(5);
	let y: Option<f64> = Some(5f64);

	println!("{:?}", y);

	let int_origin = Point{x:0,y:0};
	let float_origin = Point{x:0.0, y:0.0};

}

fn takes_anything<T>(x: T){
	//do something with x
}

fn takes_two_of_the_same_things<T>(x: T,y: T){
	// ...
}

fn takes_two_things<T,U>(x:T,y:U){
	//...
}

struct Point<T>{
	x:T,
	y:T,
}

impl<T> Point<T>{
	fn swap(&mut self){
		std::mem::swap(&mut self.x,&mut self.y);
	}
}

